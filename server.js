const db = require('./src/config/config'),
    express = require("express"),
    router = require("./src/routes"),
    cors = require("cors"),
    port = process.env.REACT_APP_PORT || 3000;

const app = express();

db.authenticate().then(() => {
    console.log('Connection has been established successfully.');
}).catch((error) => {
    console.error('Unable to connect to the database: ', error);
});

app.use(cors());
app.use(express.json());
// Import de notre router
const ROUTER = require('./src/routes')
app.use('/api', ROUTER)
app.use("/api/assets", express.static("public"));

app.listen(port, () => console.log('Le server tourne sur le port ' + port));