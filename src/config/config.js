require('dotenv').config();
const Sequelize = require("sequelize");

const db = new Sequelize(
    process.env.REACT_APP_DATABASE,
    process.env.REACT_APP_USERDB,
    process.env.REACT_APP_PASSWORD,
    {
        host: process.env.REACT_APP_HOST,
        dialect: 'mysql'
    }
);

module.exports = db;