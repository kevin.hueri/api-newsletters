const Models = require("../models");

exports.getAllRelationsTitreParties = async (req, res) => {
    try {
        const db = await Models.Titres.findAll(
            {
                include: [{
                    model: Models.Parties,
                    as: 'parties',
                    include: [{
                        model: Models.SousParties,
                        as: 'sousparties',
                    }]
                }], 
                order: [[Models.Parties, {model: Models.SousParties, as: 'sousparties'},'id', 'ASC']]
            }
        );
        res.json({
            message: "Voici vos titres avec ses parties",
            db
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.getOneRelationsTitreParties = async (req, res) => {
    try {
        const db = await Models.Titres.findByPk(req.params.id,
            {
                include: [{
                    model: Models.Parties,
                    include: {
                        model: Models.SousParties
                    }
                }]
            }
        );
        res.json({
            message: "Voici votre titre avec ses parties",
            db
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.getAllRelationsPartiesSousPartie = async (req, res) => {
    try {
        const db = await Models.Parties.findAll({ include: [{ model: Models.SousParties }] });
        res.json({
            message: "Voici vos parties avec ses sous parties",
            db
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.getOneRelationsPartiesSousPartie = async (req, res) => {
    try {
        const db = await Models.Parties.findByPk(req.params.id,
            { include: [{ model: Models.SousParties }] }
        );
        res.json({
            message: "Voici votre partie avec ses sous parties",
            db
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}