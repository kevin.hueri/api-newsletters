/*
 * On déclare nos constante
 * ************************ */
// import nodemailer
const nodemailer = require("nodemailer");

require("dotenv").config();

// Déclaration du module de connection à notre Gmail (transporteur)
let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    service: "gmail",
    port: 587,
    secure: false,
    auth: {
        user: process.env.USERMAIL,
        pass: process.env.PASSMAIL,
    },
});

var mailOptions;

module.exports = {
    mailSend: (req, res) => {
        console.log('mail', req.body)
        mailOptions = {
            from: process.env.USERMAIL,
            to: req.body.mail,
            subject: req.body.objet,
            html: `
            <html>
                <head>
                    <style>
                        body {
                            background: #DCDCDC;
                        }
                    </style>
                </head>
                <body>
                    ${req.body.html}
                </body>
            </html>
            `,
        };
        // On demande à notre transporter d'envoyer notre mail
        transporter.sendMail(mailOptions, (err, info) => {
            // console.log('mailOptions', mailOptions, transporter)
            if (err) {
                console.log("err", err),
                    res.status(500).send({
                        message: err.message || "Une erreur est survenue",
                    });
            } else {
                return res.json({
                    method: req.method,
                    status: "success",
                    flash: "Message envoyé",
                    mailoptions: mailOptions,
                });
            }
        });

    }
};