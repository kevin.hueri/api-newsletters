const Models = require("../models");

exports.getAllPartie = async (req, res) => {
    try {
        const dbpartie = await Models.Parties.findAll();
        res.json({
            message: "Voici vos parties",
            dbpartie
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.getOnePartie = async (req, res) => {
    try {
        const partieID = await Models.Parties.findByPk(req.params.id);
        res.json({
            message: "Voici votre partie",
            partieID
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.createPartie = async (req, res) => {
    try {
        await Models.Parties.create(req.body);
        const dbpartie = await Models.Parties.findAll();
        res.json({
            message: "Partie créée avec succes !",
            dbpartie
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.updatePartie = async (req, res) => {
    try {
        await Models.Parties.update(req.body, {
            where: { id: req.params.id }
        });
        const partieID = await Models.Parties.findAll({
            where: { id: req.params.id }
        });
        res.json({
            "message": "Partie changé",
            partieID
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.deletePartie = async (req, res) => {
    try {
        await Models.Parties.destroy({
            where: { id: req.params.id }
        });
        const dbpartie = await Models.Parties.findAll();
        res.json({
            "message": "Partie supprimé",
            dbpartie
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.deleteAllPartie = async (req, res) => {
    try {
        await Models.Parties.destroy({
            truncate: true
        });
        res.json({
            "message": "Parties supprimés",
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}