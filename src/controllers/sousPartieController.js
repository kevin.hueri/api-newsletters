const Models = require("../models");

exports.getAllSousPartie = async (req, res) => {
    try {
        const dbsousPartie = await Models.SousParties.findAll();
        res.json({
            message: "Voici vos sous parties",
            dbsousPartie
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.getOneSousPartie = async (req, res) => {
    try {
        const sousPartieID = await Models.SousParties.findByPk(req.params.id);
        res.json({
            message: "Voici votre sous partie",
            sousPartieID
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.createSousPartie = async (req, res) => {
    try {
        await Models.SousParties.create(req.body);
        const dbsousPartie = await Models.SousParties.findAll();
        res.json({
            message: "Sous partie créée avec succes !",
            dbsousPartie
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.updateSousPartie = async (req, res) => {
    try {
        await Models.SousParties.update(req.body, {
            where: { id: req.params.id }
        });
        const sousPartieID = await Models.SousParties.findAll({
            where: { id: req.params.id }
        });
        res.json({
            "message": "Sous partie changé",
            sousPartieID
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.deleteSousPartie = async (req, res) => {
    try {
        await Models.SousParties.destroy({
            where: { id: req.params.id }
        });
        const dbsousPartie = await Models.SousParties.findAll();
        res.json({
            "message": "Sous partie supprimé",
            dbsousPartie
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.deleteSousPartieParPartyId = async (req, res) => {
    try {
        await Models.SousParties.destroy({
            where: { partyId: req.params.id }
        });
        const dbsousPartie = await Models.SousParties.findAll();
        res.json({
            "message": "Sous partie supprimé",
            dbsousPartie
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.deleteAllSousPartie = async (req, res) => {
    try {
        await Models.SousParties.destroy({
            truncate: true
        });
        res.json({
            "message": "Sous parties supprimés",
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}