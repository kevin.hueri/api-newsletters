const Models = require("../models");

exports.getAllTitre = async (req, res) => {
    try {
        const dbtitres = await Models.Titres.findAll();
        res.json({
            message: "Voici vos titres",
            dbtitres
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.getOneTitre = async (req, res) => {
    try {
        const titreID = await Models.Titres.findByPk(req.params.id);
        res.json({
            message: "Voici votre titre",
            titreID
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.createTitre = async (req, res) => {
    try {
        await Models.Titres.create(req.body);
        const dbtitres = await Models.Titres.findAll();
        res.json({
            message: "Titre créée avec succes !",
            dbtitres
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.updateTitre = async (req, res) => {
    try {
        await Models.Titres.update(req.body, {
            where: { id: req.params.id }
        });
        const titreID = await Models.Titres.findAll({
            where: { id: req.params.id }
        });
        res.json({
            "message": "Titre changé",
            titreID
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.deleteTitre = async (req, res) => {
    try {
        await Models.Titres.destroy({
            where: { id: req.params.id }
        });
        const dbtitres = await Models.Titres.findAll();
        res.json({
            "message": "Titre supprimé",
            dbtitres
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}

exports.deleteAllTitre = async (req, res) => {
    try {
        await Models.Titres.destroy({
            truncate: true
        });
        res.json({
            "message": "Titres supprimés",
        });
    } catch (error) {
        res.json({ message: error.message });
    }
}