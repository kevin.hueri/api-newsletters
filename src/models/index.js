const db = require("../config/config");
const { Sequelize, DataTypes } = require("sequelize");

const Titres = db.define('titres', {
  titre: {
    type: DataTypes.STRING
  }
});

const Parties = db.define('parties', {
  partie: {
    type: DataTypes.STRING
  },
});

const SousParties = db.define('sousparties', {
  sousTitre: {
    type: DataTypes.STRING
  },
  description: {
    type: DataTypes.STRING
  },
  lien: {
    type: DataTypes.STRING
  },
})

Titres.hasMany(Parties);
Parties.belongsTo(Titres);

Parties.hasMany(SousParties);
SousParties.belongsTo(Parties);


exports.Titres = Titres;
exports.Parties = Parties;
exports.SousParties = SousParties;
