const express = require("express"),
    router = express.Router();

const TitreControllers = require("../controllers/titreController");
const PartieControllers = require("../controllers/partieController");
const SousPartieControllers = require('../controllers/sousPartieController');
const RelationsControllers = require("../controllers/RelationsController");
const NodemailerControllers = require('../controllers/nodemailerController')

router
    // Titres
    .get('/titres', TitreControllers.getAllTitre)
    .get('/titres/:id', TitreControllers.getOneTitre)
    .post('/titres', TitreControllers.createTitre)
    .put('/titres/:id', TitreControllers.updateTitre)
    .delete('/titres/:id', TitreControllers.deleteTitre)
    .delete('/titres', TitreControllers.deleteAllTitre)

    // Parties
    .get('/parties', PartieControllers.getAllPartie)
    .get('/parties/:id', PartieControllers.getOnePartie)
    .post('/parties', PartieControllers.createPartie)
    .put('/parties/:id', PartieControllers.updatePartie)
    .delete('/parties/:id', PartieControllers.deletePartie)
    .delete('/parties', PartieControllers.deleteAllPartie)

    // Sous parties
    .get('/sous-parties', SousPartieControllers.getAllSousPartie)
    .get('/sous-parties/:id', SousPartieControllers.getOneSousPartie)
    .post('/sous-parties', SousPartieControllers.createSousPartie)
    .put('/sous-parties/:id', SousPartieControllers.updateSousPartie)
    .delete('/sous-parties/:id', SousPartieControllers.deleteSousPartie)
    .delete('/sous-parties', SousPartieControllers.deleteAllSousPartie)
    .delete('/sous-partie-par-partyid/:id', SousPartieControllers.deleteSousPartieParPartyId)

    // Relation titre parties
    .get('/titre-parties', RelationsControllers.getAllRelationsTitreParties)
    .get('/titre-parties/:id', RelationsControllers.getOneRelationsTitreParties)

    // Relation partie sous parties
    .get('/partie-sousparties', RelationsControllers.getAllRelationsPartiesSousPartie)
    .get('/partie-sousparties/:id', RelationsControllers.getOneRelationsPartiesSousPartie)

    // Mailing
    .post('/mailing', NodemailerControllers.mailSend),

module.exports = router;